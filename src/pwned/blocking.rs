// Copyright 2020 astonbitecode
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
use tokio::runtime::Runtime;

use crate::errors;
use crate::pwned::{self, PwnedResponse};

/// Returns Ok(true) if the password is found in the [pwned passwords list](https://www.troyhunt.com/ive-just-launched-pwned-passwords-version-2/).
/// The call leverages the [k-anonimity API](https://blog.cloudflare.com/validating-leaked-passwords-with-k-anonymity/) and therefore, the password is not used in the API call in any form (not even hashed).
pub fn is_pwned(pass: &str) -> errors::Result<bool> {
    let f = pwned::is_pwned(pass);
    Runtime::new().unwrap().block_on(f)
}

/// Returns a Result<PwnedResponse> as a result for whether the password is found in the [pwned passwords list](https://www.troyhunt.com/ive-just-launched-pwned-passwords-version-2/).
/// The call leverages the [k-anonimity API](https://blog.cloudflare.com/validating-leaked-passwords-with-k-anonymity/) and therefore, the password is not used in the API call in any form (not even hashed).
pub fn check_pwned(pass: &str) -> errors::Result<PwnedResponse> {
    let f = pwned::check_pwned(pass);
    Runtime::new().unwrap().block_on(f)
}

#[cfg(test)]
mod pwned_blocking_unit_tests {
    use super::*;

    #[test]
    fn test_is_pwned() {
        let res = is_pwned("test");
        assert!(res.is_ok() && res.unwrap());
    }

    #[test]
    fn test_check_pwned() {
        let res = check_pwned("test");
        assert!(res.is_ok());
        let presp = res.unwrap();
        match presp {
            PwnedResponse::Ok => assert!(false),
            PwnedResponse::Pwned(_) => assert!(true),
        }
    }
}