// Copyright 2020 astonbitecode
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*!
# rs-password-utils

![pipeline](https://gitlab.com/astonbitecode/rs-password-utils/badges/master/pipeline.svg)

This library contains password utilities and is written in [Rust](https://www.rust-lang.org/).

Currently it offers:

* Checking passwords against [pwned passwords list](https://www.troyhunt.com/ive-just-launched-pwned-passwords-version-2/).

The check implementation leverages the [k-anonimity API](https://blog.cloudflare.com/validating-leaked-passwords-with-k-anonymity/) and therefore, the password is not used in the API calls in any form (not even hashed).

* [Diceware](https://theworld.com/~reinhold/diceware.html) for generating passphrases

## Usage

The utility can be used as a library, or as an executable:

### Library

```rust
extern crate rs_password_utils;
use std::result::Result;

#[tokio::main]
async fn main() -> Result<(), rs_password_utils::PasswordUtilsError> {
    let is_pwned = rs_password_utils::pwned::is_pwned("test").await?;

    println!("The password is pwned: {}", is_pwned);

    Ok(())
}
```

### Executable

Having rust [installed](https://www.rust-lang.org/tools/install), you may install rs-password-utils using cargo:

* From crates.io:
`cargo install rs-password-utils --features executable`

* From gitlab:
`cargo install --git https://gitlab.com/astonbitecode/rs-password-utils.git --features executable`

When the installation completes, issuing the following command:

`rs-password-utils --help`

should give you an output like:

```bash
Password utilities, written in Rust.

 Usage:
   rs-password-utils pwned
   rs-password-utils dice [(-w <count>)]
   rs-password-utils [-h]

 Options:
   -w --words    The amount of words that should comprise the passphrase
   -h --help     Show this screen.
```

## License

At your option, under:

* Apache License, Version 2.0, (http://www.apache.org/licenses/LICENSE-2.0)
* MIT license (http://opensource.org/licenses/MIT)
*/

extern crate hyper;
extern crate futures;
extern crate tokio;
extern crate hyper_tls;
extern crate sha1;
extern crate hex;
extern crate rand;

pub mod pwned;
pub mod dice;
mod errors;
pub use errors::PasswordUtilsError;