// Copyright 2020 astonbitecode
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
use std::{fmt, result};
use std::num::ParseIntError;
use std::str::Utf8Error;
use std::io;

use hex::FromHexError;
use hyper;
use hyper::http::uri::InvalidUri;

pub type Result<T> = result::Result<T, PasswordUtilsError>;

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum PasswordUtilsError {
    General(String),
    CommunicationWithThirdPartyApiError(String),
    ParseError(String),
}

impl fmt::Display for PasswordUtilsError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &PasswordUtilsError::General(ref message) => write!(f, "{}", message),
            &PasswordUtilsError::CommunicationWithThirdPartyApiError(ref message) => write!(f, "{}", message),
            &PasswordUtilsError::ParseError(ref message) => write!(f, "{}", message),
        }
    }
}

impl From<hyper::Error> for PasswordUtilsError {
    fn from(err: hyper::Error) -> PasswordUtilsError {
        PasswordUtilsError::CommunicationWithThirdPartyApiError(format!("{:?}", err))
    }
}

impl From<InvalidUri> for PasswordUtilsError {
    fn from(err: InvalidUri) -> PasswordUtilsError {
        PasswordUtilsError::CommunicationWithThirdPartyApiError(format!("{:?}", err))
    }
}

impl From<Utf8Error> for PasswordUtilsError {
    fn from(err: Utf8Error) -> PasswordUtilsError {
        PasswordUtilsError::ParseError(format!("{:?}", err))
    }
}

impl From<FromHexError> for PasswordUtilsError {
    fn from(err: FromHexError) -> PasswordUtilsError {
        PasswordUtilsError::ParseError(format!("{:?}", err))
    }
}

impl From<ParseIntError> for PasswordUtilsError {
    fn from(err: ParseIntError) -> PasswordUtilsError {
        PasswordUtilsError::ParseError(format!("{:?}", err))
    }
}

impl From<io::Error> for PasswordUtilsError {
    fn from(err: io::Error) -> PasswordUtilsError {
        PasswordUtilsError::General(format!("{:?}", err))
    }
}